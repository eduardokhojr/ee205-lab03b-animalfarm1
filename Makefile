###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author eduardo kho jr <eduardok@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   2/1/2021
###############################################################################

all: animalfarm

animals.o:    animals.c animals.h
	$(CC)    $(CFLAGS) -c animals.c

cat.o:    cat.c    cat.h
	$(CC)    $(CFLAGS) -c cat.c

main.o:    main.c cat.h
	$(CC)    $(CFLAGS) -c main.c

animalfarm:    animals.o cat.o main.o 
	gcc -o animalfarm *.c
