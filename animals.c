///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   2/1/2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color){
	switch(color){

      case BLACK  :
        return "BLACK";
      case WHITE  :
        return "WHITE";
      case RED    :
        return "RED";
      case BLUE   :
        return "BLUE";
      case GREEN  :
        return "GREEN";
      case PINK   :
        return "PINK";
   }
}
char* genderstring (enum Gender gender){


   switch(gender){

      case MALE   :
         return "MALE";
      case FEMALE :
         return "FEMALE";
   }

}

char* breedstring (enum CatBreeds breed){

   switch(breed){

      case MAIN_COON :
         return "MAIN COON";
      case MANX      :
         return "MANX";
      case SHORTHAIR :
         return "SHORTHAIR";
      case PERSIAN   :
         return "PERSIAN";
      case SPHYNX    :
         return "SPHYNX";

   }
}

char* isFixedstring (bool fixed){

   if(fixed == 1)
      return "Yes";
   else
      return "No";
}
   // @todo Map the enum Color to a string
   //return NULL;


